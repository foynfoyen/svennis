<?php

class Granavolden {


    public static function getUrl() {
        $uri = $_SERVER['REQUEST_URI'];
        $segments = explode('/',$uri);
        $template = 'frontpage';
        $seg_1 = count($segments)-1;
        $seg_2 = count($segments)-2;
        if ( count($segments) > 2 ) {
            if ( empty($segments[$seg_1]) ) {
                if ( self::getTemplate($segments[$seg_2]) ) {
                    $template = self::getTemplate($segments[$seg_2]);
                }
            } else {
                if ( strpos($segments[$seg_1], "?") ) {
                    if ( self::getTemplate(strstr($segments[$seg_1], "?", true)) ) {
                        $template = self::getTemplate(strstr($segments[$seg_1], "?", true));
                    }
                } else if ( self::getTemplate($segments[$seg_1]) ) {
                    $template = self::getTemplate($segments[$seg_1]);
                }
            }
        } else {
            if ( strpos($segments[$seg_1], "?") ) {
                if ( self::getTemplate(strstr($segments[$seg_1], "?", true)) ) {
                    $template = self::getTemplate(strstr($segments[$seg_1], "?", true));
                }
            } else if ( self::getTemplate($segments[$seg_1]) ) {
                $template = self::getTemplate($segments[$seg_1]);
            }
        }
        return $template;
    }


    public static function getTemplate($name) {
        $structureArray = self::getStructure();
        if ( array_key_exists(strtolower($name), $structureArray) ) {
            return $structureArray[strtolower($name)]['template'];
        } else {
            return false;
        }
    }


    public static function getStructure() {
        $defaultIntro = '<p>Lorem ipsum Pariatur dolor in deserunt est mollit cillum eiusmod adipisicing Duis laboris nisi. Consectetur adipiscing elit. Ut felis enim, bibendum eget enim id, suscipit imperdiet ligula. </p><p>At volutpat nulla luctus facilisis. Quisque vestibulum sapien et vulputate. Phasellus at tortor rhoncus, consectetur odio id, vulputate mi.</p>';
        $defaultBody = '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut felis enim, bibendum eget enim id, suscipit imperdiet ligula. Nulla aliquam condimentum commodo. Sed imperdiet pharetra tellus, vel commodo mauris euismod ultricies. Proin a mauris lacus. Etiam non dui non nibh hendrerit consectetur. Curabitur rutrum nisl erat, vitae interdum urna consectetur tempor. Aenean non arcu sodales nisi pellentesque mollis et id nisl. Curabitur placerat tempus ultricies. Aenean iaculis ante id felis consectetur consectetur quis vitae dolor. </p>';
        $defaultBody .= '<p>Duis vehicula sit amet neque quis molestie. Nullam sed vestibulum tortor. Proin condimentum orci ante, at volutpat nulla luctus facilisis. Quisque vestibulum sapien et vulputate pulvinar. Phasellus at tortor rhoncus, consectetur odio id, vulputate mi. Sed vitae quam ante. Sed sit amet fermentum diam. Donec rutrum imperdiet vehicula. Maecenas elementum nisi a mi adipiscing elementum. Proin ullamcorper consectetur magna, in aliquam dolor aliquam a. Fusce sed bibendum neque. Pellentesque rutrum leo in porttitor porttitor. Phasellus cursus ipsum a lorem dictum viverra. Curabitur eleifend risus eu nibh adipiscing tempus. Ut mauris diam, feugiat eget iaculis nec, consequat eget lacus. Curabitur volutpat erat at urna adipiscing, ut facilisis turpis tempor. </p>';
        $defaultBody .= '<p>Ut sit amet turpis ac odio commodo scelerisque sit amet at enim. In mollis augue ut blandit imperdiet. Duis blandit eleifend urna non fringilla. Curabitur in accumsan ligula, ut iaculis eros. Aenean pellentesque, ligula quis sagittis pulvinar, mi libero consectetur risus, non consequat ligula sapien sed libero. Integer molestie lobortis magna ac fermentum. In ac viverra velit. Nulla facilisi. Aliquam ultricies augue at diam ultrices suscipit. Praesent id nunc at risus molestie congue a non augue. Curabitur sagittis tincidunt eros, in adipiscing mauris. Sed gravida lectus ac metus convallis commodo. </p>';
        $defaultImage = '/images/storage/full/vinter.jpg';
        return $structureArray = array(
            'konferanser' => array(
                'showmenu' => true,
                'template' => 'page',
                'title' => 'Konferanser',
                'english_title' => 'Conferences',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'body' => $defaultBody
            ),
            'selskaper' => array(
                'showmenu' => true,
                'template' => 'page',
                'title' => 'Selskaper',
                'english_title' => 'Banquets',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'body' => $defaultBody,
                'children' => array(
                    'bryllup' => 'Bryllup'
                )
            ),
            'overnatting' => array(
                'showmenu' => true,
                'template' => 'page',
                'title' => 'Overnatting',
                'english_title' => 'Accomodation',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'body' => $defaultBody
            ),
            'om-gjestgiveriet' => array(
                'showmenu' => true,
                'template' => 'page',
                'title' => 'Om gjestgiveriet',
                'english_title' => 'About',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'body' => $defaultBody,
                'children' => array(
                    'historie' => 'Historie',
                    'fasiliteter' => 'Fasiliteter',
                    'restauranten' => 'Restauranten',
                    'omradet-rundt-gjestgiveriet' => 'Området rundt gjestgiveriet'
                )
            ),
            'kontakt' => array(
                'showmenu' => true,
                'template' => 'contact',
                'title' => 'Kontakt',
                'english_title' => 'Contact',
                'intro' => '<p>Lorem ipsum Pariatur incididunt eu sit pariatur qui nisi aute ea nisi velit quis laboris in Duis enim occaecat Excepteur velit in nulla non nostrud laborum ex quis.</p>',
                'image' => $defaultImage,
                'body' => ''
            ),
            'historie' => array(
                'showmenu' => false,
                'template' => 'page',
                'title' => 'Historie',
                'alt_title' => 'Stedet der historien beveger deg',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'embed_image' => '/images/storage/embed/historien.png',
                'body' => $defaultBody
            ),
            'fasiliteter' => array(
                'showmenu' => false,
                'template' => 'page',
                'title' => 'Fasiliteter',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'body' => $defaultBody
            ),
            'restauranten' => array(
                'showmenu' => false,
                'template' => 'page',
                'title' => 'Restauranten',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'body' => $defaultBody
            ),
            'omradet-rundt-gjestgiveriet' => array(
                'showmenu' => false,
                'template' => 'page',
                'title' => 'Området rundt gjestgiveriet',
                'intro' => $defaultIntro,
                'image' => $defaultImage,
                'embed_image' => '/images/storage/embed/opplevelser.png',
                'body' => $defaultBody
            ),
            'bryllup' => array(
                'showmenu' => false,
                'template' => 'page',
                'title' => 'Bryllup',
                'alt_title' => 'Granavolden Gjæstegiveri passer perfekt til bryllup',
                'intro' => '<p>Lorem ipsum Pariatur dolor in deserunt est mollit cillum eiusmod adipisicing Duis laboris nisi. Lorem ipsum Quis sint minim sint deserunt consectetur id ad consectetur culpa mollit aute ut in Ut nostrud est minim nostrud do cupidatat cupidatat dolore eiusmod ex Ut tempor ex.</p>',
                'image' => '/images/storage/full/bryllup.jpg',
                'embed_image' => '/images/storage/embed/bryllup.png',
                'body' => $defaultBody
            ),
        );
    }


    public static function getPage() {
        $uri = $_SERVER['REQUEST_URI'];
        $segments = explode('/',$uri);
        if ( count($segments) > 2 ) {
            if ( strpos($segments[count($segments)-1], "?") ) {
                $test = strstr($segments[count($segments)-1], "?", true);
                $test2 = strstr($segments[count($segments)-2], "?", true);
                if ( empty($test) ) {
                    $page = $test2;
                } else {
                    $page = $test;
                }
            } else if ( empty($segments[count($segments)-1]) ) {
                $page = $segments[count($segments)-2];
            } else {
                $page = $segments[count($segments)-1];
            }
        } else {
            if ( strpos($segments[count($segments)-1], "?") ) {
                $test = strstr($segments[count($segments)-1], "?", true);
                $test2 = strstr($segments[count($segments)-2], "?", true);
                if ( empty($test) ) {
                    $page = $test2;
                } else {
                    $page = $test;
                }
            } else {
                $page = $segments[count($segments)-1];
            }
        }
        return $page;
    }


}

?>
