var latval  = 60.366497;
var longval = 10.528186;

function initialize() {

    var mapOptions = {
        center: new google.maps.LatLng(latval, longval),
        zoom: 8,
        disableDefaultUI: true,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        draggable: false
    };

    var map = new google.maps.Map(document.getElementById("map-canvas"),
        mapOptions);

    var marker = new google.maps.Marker({
        position: map.getCenter(),
        icon: '/images/map_pointer.png',
        draggable: true,
        map: map
    });

    google.maps.event.addListener(map, 'click', function() {
        window.open('http://maps.google.com/?ll='+latval+','+longval+'&q='+latval+','+longval+'&z=8', '_blank');
    });

}

google.maps.event.addDomListener(window, 'load', initialize);
