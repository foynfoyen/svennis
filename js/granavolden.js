$(document).ready(function() {

    var owl = $("#owl-slider");

    owl.owlCarousel({
        singleItem      : true,
        stopOnHover     : true,
        autoPlay        : true,
        transitionStyle : "fade",
        autoHeight      : true
    });

    $("#mmenu-menu").mmenu({
        position   : 'right'
    });

    $(".pausebutton").click(function(){
        owl.trigger('owl.stop');
        $(this).hide();
        $(".playbutton").show();
    });

    $(".playbutton").click(function(){
        owl.trigger('owl.play',5000);
        $(this).hide();
        $(".pausebutton").show();
    });

    $(".control-buttons").appendTo($(".owl-controls"));

    $(".mmenu-toggler").click(function(){
        var toppx = 25 - $(window).scrollTop();
        $('.header-main').css({'position' : 'fixed'});
    });


    $('.search-field').hide();
    $('.search-button').click(function( event ) {
        event.preventDefault();
        $( this ).toggleClass('clicked').next('.search-field').toggle( 0 );
    });


    // Sticky header
    var $header = $('.header-main');
    var $logo = $header.find( '.logo' );
    $(window).bind( 'load scroll hashchange', function( event ){
        if ( $(window).scrollTop() > 25 ){
            $header.css({
                'position' : 'fixed',
                'top' : '0',
                'width' : '100%',
                'margin-top' : '0'
            });
            $logo.addClass('scrolling');
        } else {
            $header.css({'position' : 'absolute', 'top' : '25px', 'width' : '100%'});
            $logo.removeClass('scrolling');
        }
    });
});


