<?php require_once('classes/granavolden.php'); ?>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Granavolden Gjæstgiveri</title>
    <meta name="description" content="Overnatting med tilnærmet hotellstandard på Hadeland. Rimelige priser og rolige omgivelser. Rom og kurslokaler ble pusset opp i 2011." />
    <!--[if IE]>
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/css/font/fontface.css">
    <link rel="stylesheet" href="/css/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/foundation/normalize.css">
    <link rel="stylesheet" href="/css/foundation/foundation.min.css">
    <link rel="stylesheet" href="/css/jquery.mmenu.all.css" />
    <link rel="stylesheet" href="/css/owlcarousel/owl.carousel.css" />
    <link rel="stylesheet" href="/css/owlcarousel/owl.theme.css" />
    <link rel="stylesheet" href="/css/owlcarousel/owl.transitions.css" />
    <link rel="stylesheet" href="/css/granavolden.css">
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
    <script src="/js/foundation/js/vendor/modernizr.js"></script>
    <script src="/js/foundation/js/vendor/jquery.js"></script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCT_jLdIZID_pmmfN_1lFPcqa8dVy_yXPQ"></script>
    <script src="/js/jquery.mmenu.min.all.js"></script>
    <script src="/js/owlcarousel/owl.carousel.min.js"></script>
</head>

<body>
    <div id="page">
        <nav id="mmenu-menu">
            <ul>
                <?php
                foreach (Granavolden::getStructure() as $key => $value) {
                    if ( $value['showmenu'] ) {
                ?>
                    <?php if ( isset($_GET["l"]) && $_GET["l"] == "english" ) { ?>
                        <li><a href="<?php print $key; ?>"><?php print $value['english_title']; ?></a></li>
                    <?php } else { ?>
                        <li><a href="<?php print $key; ?>"><?php print $value['title']; ?></a></li>
                    <?php } ?>
                <?php
                    }
                }
                ?>
            </ul>
        </nav>
        <header class="header-main mm-fixed-top">
            <div class="row">
                <h1 class="logo">
                    <a href="/">
                        <img class="standard-logo" src="/images/logo.png" alt="Logo">
                        <img class="scrollable-logo" src="/images/logo-small.jpg" alt="Logo">
                    </a>
                </h1>
                <nav class="main-menu hide-for-medium-down">
                    <ul>
                        <?php
                        $uri = $_SERVER['REQUEST_URI'];
                        $breadcrumbs = explode('/',$uri);
                        foreach (Granavolden::getStructure() as $key => $value) {
                            if ( $value['showmenu'] ) {
                                if ( in_array($key, $breadcrumbs) ) {
                                    $class = 'active';
                                } else {
                                    $class = '';
                                }
                        ?>
                            <?php if ( isset($_GET["l"]) && $_GET["l"] == "english" ) { ?>
                                <li class="<?php print $class; ?>"><a href="/<?php print $key; ?>"><?php print $value['english_title']; ?></a></li>
                            <?php } else { ?>
                                <li class="<?php print $class; ?>"><a href="/<?php print $key; ?>"><?php print $value['title']; ?></a></li>
                            <?php } ?>
                        <?php
                            }
                        }
                        ?>
                    </ul>
                </nav>
                <ul class="sub-menu">
                    <?php if ( isset($_GET["l"]) && $_GET["l"] == "english" ) { ?>
                    <li><a href="?l=norsk">Norsk</a></li>
                    <?php } else { ?>
                    <li><a href="?l=english">English</a></li>
                    <?php } ?>
                    <li>
                        <a class="search-button" href="#"><i class="fa fa-search"></i><span>Søk</span></a>
                        <div class="search-field">
                            <form>
                                <input type="text">
                                <button class="search-button">Søk</button>
                            </form>
                        </div>
                    </li>
                    <li><a class="mmenu-toggler" href="#mmenu-menu"><span>Meny</span></a></li>
                </ul>
            </div>
        </header>
