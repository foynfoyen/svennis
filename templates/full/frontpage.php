<section class="frontpage full">
    <div id="owl-slider" class="slideshow">
        <div>
            <img data-interchange="[/images/storage/full/hovedbygget-mobile.png, (small)], [/images/storage/full/hovedbygget-tablet.png, (medium)], [/images/storage/full/hovedbygget.png, (large)]" alt="Historie">
            <div class="caption">
                <div class="row">
                    <div class="columns small-12 medium-9">
                        <a href="/om-gjestgiveriet/historie">
                            <b>Historie</b>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <img data-interchange="[/images/storage/full/foresporsel-mobile.png, (small)], [/images/storage/full/foresporsel-tablet.png, (medium)], [/images/storage/full/foresporsel.jpg, (large)]" alt="Forespørsel">
            <div class="caption">
                <div class="row">
                    <div class="columns small-12 medium-9">
                        <a href="/kontakt">
                            <b>Forespørsel</b>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <img data-interchange="[/images/storage/full/vinter-mobile.png, (small)], [/images/storage/full/vinter-tablet.png, (medium)], [/images/storage/full/vinter.jpg, (large)]" alt="Vinter">
        </div>
    </div>
    <div class="control-buttons">
        <a class="pausebutton"><i class="fa fa-pause"></i><span>Pause</span></a>
        <a class="playbutton"><i class="fa fa-play"></i><span>Play</span></a>
    </div>

    <section class="frontpage-featured">
        <div class="row">
            <div class="columns">
                <h1>
                    «Vi anbefaler stedet på det varmeste for konferanser, bryllup, helgeturer og andre arrangementer»
                </h1>
                <a class="contact-button" href="/kontakt"><i class="fa fa-envelope-o"></i>Send forespørsel</a>
            </div>
        </div>
    </section>

    <section class="main-content">
        <div class="row" data-equalizer>
            <?php
                $structure = Granavolden::getStructure();

                $content = $structure['bryllup'];
                $url = '/selskaper/bryllup';
                include 'templates/embed/page.php';

                $content = $structure['omradet-rundt-gjestgiveriet'];
                $url = '/om-gjestgiveriet/omradet-rundt-gjestgiveriet';
                include 'templates/embed/page.php';

                $content = $structure['historie'];
                $url = '/om-gjestgiveriet/historie';
                include 'templates/embed/page.php';
            ?>

        </div>
    </section>
</section>
