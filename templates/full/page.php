<?php
$structureArray = Granavolden::getStructure();
$page = Granavolden::getPage();
$content = $structureArray[strtolower($page)];
?>
<div class="page full">
    <div class="full-image">
        <img src="<?php print $content['image']; ?>" alt="">
    </div>

    <?php include 'templates/parts/breadcrumb.php'; ?>

    <section class="main-content row">

        <div class="columns">
            <h1><?php print $content['title']; ?></h1>
        </div>

        <?php if ( isset($content['children']) ) { ?>
        <aside class="side-menu columns small-12 medium-5 large-4 medium-push-7 large-push-8">
            <div>
                <ul>
                    <?php
                    foreach ($content['children'] as $key => $value) {
                        ?>
                        <li>
                            <a href="/<?php print $page; ?>/<?php print $key; ?>">
                                <i class="fa fa-angle-double-right"></i>
                                <?php print $value; ?>
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </aside>
        <?php } ?>

        <div class="columns small-12 medium-7 large-8 main-column medium-pull-5 large-pull-4">
            <div class="intro">
                <?php print $content['intro']; ?>
            </div>

            <div class="body">
                <?php print $content['body']; ?>
            </div>
        </div>

    </section>
</div>
