<?php
$structureArray = Granavolden::getStructure();
$page = Granavolden::getPage();
$content = $structureArray[strtolower($page)];
?>
<div class="contact full">
    <div class="full-image">
        <img src="<?php print $content['image']; ?>" alt="">
    </div>

    <?php include 'templates/parts/breadcrumb.php'; ?>

    <section class="main-content row">

        <div class="columns">
            <h1><?php print $content['title']; ?></h1>
        </div>

        <aside class="side-menu columns small-12 medium-5 large-4 medium-push-7 large-push-8">
            <div>
                <p>
                    <i class="fa fa-phone"></i>+47 61 33 77 00
                </p>
                <p>
                    <a href="mailto:post@granavolden.no"><i class="fa fa-envelope-o"></i>post@granavolden.no</a>
                </p>
                <br>
                <p>Post og besøksadresse:</p>
                <p>Granavollen 16, 2750 Gran</p>
            </div>
        </aside>

        <div class="columns small-12 medium-7 large-8 main-column medium-pull-5 large-pull-4">
            <div class="body">
                <?php print $content['intro']; ?>
            </div>
            <form>
                <div class="row">
                    <div class="large-3 columns">
                        <label>Dato ankomst:
                            <input type="text" />
                        </label>
                    </div>
                    <div class="large-3 columns">
                        <label>Dato avreise:
                            <input type="text" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Fornavn:
                            <input type="text" />
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Etternavn:
                            <input type="text" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Bedrift:
                            <input type="text" />
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Gateadresse:
                            <input type="text" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Postnummer:
                            <input type="text" />
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>Poststed:
                            <input type="text" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-6 columns">
                        <label>Telefonnummer:
                            <input type="text" />
                        </label>
                    </div>
                    <div class="large-6 columns">
                        <label>E-post:
                            <input type="text" />
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="large-12 columns">
                        <label>Annen informasjon
                            <textarea rows="8"></textarea>
                        </label>
                    </div>
                </div>
                <div class="large-3 columns">
                    <label class="checkbox-label" for="konferanse">Konferanse:</label><input id="konferanse" type="checkbox">
                </div>
                <div class="large-3 columns">
                    <label class="checkbox-label" for="selskap">Selskap:</label><input id="selskap" type="checkbox">
                </div>
                <div class="large-3 columns">
                    <label class="checkbox-label" for="overnatting">Overnatting:</label><input id="overnatting" type="checkbox">
                </div>
                <div class="large-3 columns">
                    <label class="checkbox-label" for="annet">Annet:</label><input id="annet" type="checkbox">
                </div>
                <button>Send</button>
            </form>
        </div>

    </section>
</div>
