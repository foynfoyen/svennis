<figure class="google-maps">
    <div id="map-canvas"></div>
</figure>
<footer class="footer-main">
    <div class="row">
        <div class="columns small-12 medium-4">
            <p>
                <i class="fa fa-phone"></i>+47 61 33 77 00
            </p>
            <p>
                <a href="mailto:post@granavolden.no"><i class="fa fa-envelope-o"></i>post@granavolden.no</a>
            </p>
        </div>
        <div class="columns small-12 medium-4">
            <p>Post og besøksadresse:</p>
            <p>Granavollen 16, 2750 Gran</p>
        </div>
        <div class="columns small-12 medium-4">
            <p>
                <i class="fa fa-facebook-square"></i>
                <a href="https://www.facebook.com/pages/Granavolden-Gj%C3%A6stgiveri/374542652556427">
                    Følg oss på Facebook
                </a>
            </p>
        </div>
    </div>
</footer>

<script src="/js/foundation/js/foundation.min.js"></script>
<script>
    $(document).foundation();
</script>
<script src="/js/granavolden.js"></script>
<script src="/js/maps.js"></script>
</div>
</body>
</html>
