<?php
$uri = $_SERVER['REQUEST_URI'];
$breadcrumbs = explode('/',$uri);
$structure = Granavolden::getStructure();
$currentPage = Granavolden::getPage();
?>
<div class="row breadcrumb-row">
    <ul class="breadcrumb">
        <li><a href="/">Forside</a></li>
        <?php
        $url = false;
        foreach ($breadcrumbs as $breadcrumb) {
            if ( !empty($breadcrumb) ) {
                if ( strpos($breadcrumb, "?") ) {
                    $url .= '/' . strstr($breadcrumb, "?", true);
                    $title = $structure[strstr($breadcrumb, "?", true)]['title'];
                } else {
                    $url .= '/' . $breadcrumb;
                    $title = $structure[$breadcrumb]['title'];
                }

                if ($breadcrumb === $currentPage) {
                    ?>
                    <li class="active"><i class="fa fa-angle-right"></i><?php print $title ?></li>
                    <?php
                } else {
                    ?>
                    <li><i class="fa fa-angle-right"></i><a href="<?php print $url; ?>"><?php print $title ?></a></li>
                    <?php
                }
            }
        }
        ?>
    </ul>
</div>
