<article class="columns small-12 medium-4 embed page" data-equalizer-watch>
    <div class="page-wrapper">
        <a href="<?php print $url ?>">
            <img src="<?php print $content['embed_image']; ?>" alt="">
        </a>
        <?php if (isset($content['alt_title'])) {
            ?>
            <h1><a href="<?php print $url ?>"><?php print $content['alt_title']; ?></a></h1>
            <?php
        } else {
            ?>
            <h1><a href="<?php print $url ?>"><?php print $content['title']; ?></a></h1>
            <?php
        }
        ?>
        <?php print $content['intro']; ?>
    </div>
</article>
