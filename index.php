<?php

/* Page header */
require_once 'templates/page_header.php';


$template = Granavolden::getUrl();
if ( $template && file_exists('templates/full/' . $template . '.php') ) {
    include 'templates/full/' . $template . '.php';
}


/* Page footer */
require_once 'templates/page_footer.php';


error_reporting(E_ERROR | E_PARSE);

?>

